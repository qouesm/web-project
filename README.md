# CPS493 Web Development Project<br>**Chart Maker**

# `05/17/22: `**`REPOSITORY IS SUITIBLE FOR GRADING`**

## Closing thoughts

This project is not _done_, and I'm not done with it, but time has run out for me to keep working.

Chart functionality is baseline and working but that's about the most I can say.
While you can currently register, sign in, and delete user accounts, no other database functionality is implemented.
In it's current state, the database essentially exists as a placeholder for features to come.

I'm definitely happy with the front end, even as limited as it is now, but back end development has not gone nearly as smoothly.
I have learned a ton from working on this project and I'm going to need to learn a lot more to release this as a service.

## Future plans

Moving forward, I do want to complete this project in the future with some different tools (If not just for the fact that much of the server was rushed).
I plan to scrap the current server and replace it with a [Go](https://go.dev/) server, forgoing a REST API (currently [ExpressJS](http://expressjs.com/)) for [GraphQL](https://graphql.org/) and writing manual SQL queries with an ORM like [GORM](https://gorm.io/).

[Svelte](https://svelte.dev/) and [TailwindCSS](https://tailwindcss.com/) have been great to work with, though Javascript can often difficult to work with, something that [Typescript](https://typescriptlang.org) aliveates to an extent.
The [Last.fm NPM package](https://npmjs.com/package/last-fm) is a poignant example of Javascript being be a pain, as it's a library that does not provide a type definitions package like the [most popular packages do](https://npmjs.com/package/@types/express).

---

## What is this?

This project was made for my web development class.
It is a full stack web app for creating, saving, and sharing collages of lists of music albums.
Heavy inspiration is drawn from [Topsters](https://www.neverendingchartrendering.org/)
with the goal of creating a more feature complete product.

![maker.gif](doc/img/maker.gif)

This project is built with:

-   [Svelte](https://svelte.dev/) ([SvelteKit](https://kit.svelte.dev/))
-   [TailwindCSS](https://tailwindcss.com/)
-   [Typescript](https://typescriptlang.org/)
-   [Express](https://expressjs.com/)
-   [PostgreSQL](https://postgresql.org/)
-   [Last.fm API](https://last.fm/api)
<!-- - Axios -->

## Functionality

The main functionality is on the 'Maker' page.
The grid of cells are cards and there is a utility sidebar on the left side.

First, use the search to find albums with the [Last.fm API](https://www.last.fm/api). You can drag albums into the card grid, rearrange them, and delete them by dragging them back to the search area.

With the options menu, you can customize the look of the chart.
You can change the size of the chart, the card magins, and include album information.
The chart state is persistent through resizes and page reloads (though some cards may be visually cut off).

### UNIMPLEMENTED

Also in this menu, you can switch between the different charts associated with your user account.
In the final menu, you can name these charts and give them a fitting description.

Charts can then be saved to a user account and exported into various file formats (PNG, JSON, TXT, etc.).

## Bugs/Important Information

-   NOTE: It may be worth noting that 90% of testing was done on Firefox, the other 10% on Chromium. Chromium may have bugs and other browsers are untested.

-   _ISSUE_: Card animations have been removed for now.

-   **MAJOR**: Charts are not saved to the database.
    While chart state is persistent on the front end (via localStorage), I was not able to save this information given the time constraints of the course.

## Running locally

### Depends on:

```
nodejs      (>v8.1.2)
npm         (>v16.13.2)
postgresql  (>v14)
```

### Setting up the database:

With PostgreSQL, create the database with the following command:

```sql
CREATE DATABASE chartmaker;
```

The name of this database must match `PG_DATABASE` in `server/.env` (covered later).

Tables will automatically be created when the server is started.

_BUG: The server may need to be started more than once to create the tables._
_This is due to key dependancy when not every table exists yet_

### For development:

```sh
cd client
npm install
npm run dev
```

```sh
cd server
npm install
npm run dev
```

### For production:

```sh
cd client
npm install
npm run build
npm run preview
```

```sh
cd server
npm install
npm run build
npm run start
```

The following environment variables must be provided in `server/.env` (default values provided)

If `DEV` is set to `true`, tables will automatically seed themselves.

```
LAST_FM_KEY
EX_PORT=5000
PG_USERNAME
PG_PASSWORD
PG_DATABASE=chartMaker
PG_PORT=5432
(optional) DEV=false (boolean)
```


## Pictures

### Maker

![maker1](doc/img/maker1.png)
![maker2](doc/img/maker2.png)

### With light mode

![light](doc/img/light.png)

### On a phone

![phone](doc/img/phone.png)

### Sign in page

![dark](doc/img/signin.png)

### Sidebar (while signed in)

![sidebar](doc/img/sidebar.png)
