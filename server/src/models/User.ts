import { QueryResult } from 'pg';
import Database from '../utils/Database';
import Chart from './Chart';
import Options from './Options';

export default class User {
    userId: string;
    username: string;
    password?: string;
    charts?: Chart[];

    constructor(username: string, password?: string, userId?: string) {
        this.username = username;
        this.password = password;
        this.charts = [new Chart(`${username}'s chart`, '')];
        this.userId = userId ?? '';
    }

    async signIn(): Promise<User> {
        if (!(await this.exists())) throw { message: 'User not found' };

        const user = await this.get();
        if (this.password !== user.password)
            throw { message: 'Password is incorrect' };
        return user;
    }

    async create(): Promise<User> {
        if (await this.exists()) throw { message: 'User already exists' };
        await Database.query(
            `INSERT INTO users (username, password) VALUES ($1, $2)`,
            [this.username, `${this.password}`]
        ).catch((err) => {
            throw console.error('Could not register user', err);
        });

        return await this.get();
    }

    async delete() {
        const user: User = await this.get();
        if (!user) throw { message: 'User not found' };
        await Database.query(`DELETE FROM users WHERE user_id = ($1)`, [
            user.userId,
        ]).catch((err) => {
            throw console.error('Could not delete user', err);
        });
    }

    async get(): Promise<User> {
        if (this.userId) {
            const q: QueryResult = await Database.query(
                `SELECT * FROM users WHERE user_id = ($1)`,
                [this.userId]
            );
            const user: User = new User(q.rows[0].username, q.rows[0].password);
            user.userId = q.rows[0].user_id;
            return user;
        } else {
            const q: QueryResult = await Database.query(
                `SELECT * FROM users WHERE username = ($1)`,
                [this.username]
            );
            const user: User = new User(
                q.rows[0].username,
                q.rows[0].password,
                q.rows[0].user_id
            );
            return user;
        }
    }

    async exists(): Promise<boolean> {
        try {
            const q: QueryResult = await Database.query(
                `SELECT * FROM users WHERE username = ($1)`,
                [this.username]
            );
            return !!q.rows.length;
        } catch (err) {
            console.error('Could not check user', err);
            return false;
        }
    }

    async getChart(chart: Chart): Promise<Chart> {
        if (chart.chartId) {
            const q: QueryResult = await Database.query(
                `SELECT * FROM charts WHERE user_id = ($1) AND chart_id = ($2)`,
                [this.userId, chart.chartId]
            );
            if (q.rowCount === 0) {
                throw { message: 'Chart not found' };
            }
            const res = q.rows[0];
            return new Chart(
                res.title,
                res.user_id,
                res.chart_id,
                res.description,
                res.created,
                res.edited,
                new Options(
                    res.height,
                    res.width,
                    res.margin,
                    res.show_titles,
                    res.show_numbers
                )
                // TODO get cards
            );
        } else if (chart.title) {
            const q: QueryResult = await Database.query(
                `SELECT * FROM charts WHERE user_id = ($1) AND title = ($2)`,
                [this.userId, chart.title]
            );
            if (q.rowCount === 0) {
                throw { message: 'Chart not found' };
            }
            const res = q.rows[0];
            return new Chart(
                res.title,
                res.user_id,
                res.chart_id,
                res.description,
                res.created,
                res.edited,
                new Options(
                    res.height,
                    res.width,
                    res.margin,
                    res.show_titles,
                    res.show_numbers
                )
                // TODO get cards
            );
        } else {
            throw { message: 'Chart ID or title required' };
        }
    }

    async saveChart(chart: Chart) {
        console.log('lookie', chart.cards);
        try {
            // update existing chart
            const c = await this.getChart(chart);
            if (c.chartId) {
                if (await this.getChart(chart)) {
                    await Database.query(
                        `UPDATE charts SET
                            title = ($1), 
                            description = ($2), 
                            edited = CURRENT_TIMESTAMP,    
                            height = ($3),
                            width = ($4),
                            margin = ($5),
                            show_titles = ($6),
                            show_numbers = ($7)
                            
                            WHERE user_id = ($8) AND chart_id = ($9)`,
                        [
                            chart.title,
                            chart.description,
                            chart.options.height.toString(),
                            chart.options.width.toString(),
                            chart.options.margin.toString(),
                            chart.options.showTitles.toString(),
                            chart.options.showNumbers.toString(),
                            this.userId,
                            c.chartId,
                        ]
                    );

                    // save to chart_cards
                    await Database.query(
                        `DELETE FROM chart_cards WHERE chart_id = ($1)`,
                        [c.chartId]
                    );
                    console.log('here we go');
                    // console.log(chart.cards);
                    for (const card of chart.cards) {
                        if (card.empty) continue;
                        console.log('card', card);
                        console.log('time to query');
                        await Database.query(
                            `INSERT INTO chart_cards (chart_id, card_id) VALUES ($1, $2)`,
                            [c.chartId, card.cardId]
                        )
                            .then(() => console.log('success'))
                            .catch((err) => {
                                console.error('XD', err);
                                throw { message: 'Could not save card' };
                            });
                    }
                    console.log('we are done wih that');
                }
            }
        } catch (err) {
            // create new chart
            await Database.query(
                `INSERT INTO charts (
                    user_id, 
                    title,
                    description,
                    height,
                    width,
                    margin,
                    show_titles,
                    show_numbers
                ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`,
                [
                    this.userId,
                    chart.title,
                    chart.description,
                    chart.options.height.toString(),
                    chart.options.width.toString(),
                    chart.options.margin.toString(),
                    chart.options.showTitles.toString(),
                    chart.options.showNumbers.toString(),
                ]
            );
        }
    }

    static async createTable() {
        await Database.query(
            `CREATE TABLE IF NOT EXISTS users (
                user_id SERIAL,
                username VARCHAR(32) NOT NULL UNIQUE,
                password VARCHAR(32) NOT NULL,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                CONSTRAINT user_pk PRIMARY KEY (user_id)
            )`
        );
    }

    static async seed() {
        const seeds: User[] = [
            new User('andrw', 'secretPassword'),
            new User('charl', 'myPassword'),
            new User('kaitl', 'verySecretPassword'),
        ];
        for (const user of seeds) {
            await user.create().catch(() => {});
        }
    }
}

User.createTable().then(() => {
    if (process.env.DEV) User.seed();
});
