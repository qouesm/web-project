import { QueryResult } from 'pg';
import Database from '../utils/Database';
import Card from './Card';
import Options from './Options';
import User from './User';

export default class Chart {
    chartId?: string;
    userId: string;
    title: string;
    description: string;
    created: Date;
    edited?: Date;
    cards: Card[];
    options: Options;

    constructor(
        title: string,
        userId: string,
        chartId: string = '',
        description: string = '',
        created: Date = new Date(),
        edited: Date | undefined = undefined,
        options: Options = new Options(),
        cards: Card[] = [new Card()]
    ) {
        this.chartId = chartId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.created = created;
        this.edited = edited;
        this.options = options;
        this.cards = cards;
    }

    async create() {
        if (await this.exists()) {
            throw { message: 'Chart already has chartId' };
        }

        await Database.query(
            `INSERT INTO charts (
                user_id, 
                title, 
                description, 
                height, 
                width, 
                margin, 
                show_titles, 
                show_numbers
            ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`,
            [
                this.userId,
                this.title,
                this.description,
                this.options.height.toString(),
                this.options.width.toString(),
                this.options.margin.toString(),
                this.options.showTitles ? 'TRUE' : 'FALSE',
                this.options.showTitles ? 'TRUE' : 'FALSE',
            ]
        ).catch((err) => {
            throw console.error('Could not create chart', err);
        });

        return await this.get();
    }

    async get(): Promise<Chart> {
        let q: QueryResult;
        if (this.chartId) {
            q = await Database.query(
                `SELECT * FROM charts WHERE chart_id = ($1)`,
                [this.chartId]
            );
        } else if (this.userId && this.title) {
            q = await Database.query(
                `SELECT * FROM charts WHERE user_id = ($1) AND title = ($2)`,
                [this.userId, this.title]
            );
        } else throw { message: 'Chart ID or User ID and Title are required' };
        if (q.rowCount === 0) throw { message: 'Chart does not exist' };

        const c = q.rows[0];
        const chart: Chart = new Chart(
            c.title,
            c.user_id,
            c.chart_id,
            c.description,
            c.created,
            c.edited,
            new Options(
                c.height,
                c.width,
                c.margin,
                c.show_titles === 'TRUE',
                c.show_numbers === 'TRUE'
            ),
            [new Card()] // TODO: get cards
        );

        return chart;
    }

    async exists(): Promise<boolean> {
        return await this.get()
            .then(() => true)
            .catch(() => false);
    }

    static async createTable() {
        // user_id INT AUTO_INCREMENT,
        await Database.query(
            `CREATE TABLE IF NOT EXISTS charts (
                chart_id SERIAL,
                user_id INT,
                title VARCHAR(32) NOT NULL,
                description TEXT NOT NULL,
                created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                edited TIMESTAMP,
                height INT DEFAULT 5 NOT NULL,
                width INT DEFAULT 5 NOT NULL,
                margin INT DEFAULT 4 NOT NULL,
                show_titles BOOLEAN DEFAULT FALSE NOT NULL,
                show_numbers BOOLEAN DEFAULT FALSE NOT NULL,
                CONSTRAINT chart_pk PRIMARY KEY (chart_id),
                CONSTRAINT user_fk FOREIGN KEY (user_id) REFERENCES users(user_id)
            )
        `);

        await Database.query(`
            CREATE TABLE IF NOT EXISTS chart_cards (
                chart_id INT,
                card_id VARCHAR(32) UNIQUE,
                CONSTRAINT chart_card_pk PRIMARY KEY (chart_id, card_id),
                CONSTRAINT chart_fk FOREIGN KEY (chart_id) REFERENCES charts(chart_id),
                CONSTRAINT card_fk FOREIGN KEY (card_id) REFERENCES cards(card_id)
            )
        `);
    }

    static async seed() {
        const seeds: Chart[] = [
            new Chart(
                'My Chart',
                (await new User('andrw').get()).userId,
                '',
                'this is my chart',
                undefined,
                undefined,
                new Options(10, 10, 2, true, true),
                [new Card()]
            ),
            new Chart(
                'Test Chart',
                (await new User('charl').get()).userId,
                '',
                'this is a test chart',
                undefined,
                undefined,
                new Options(3, 3, 8, false, true),
                [new Card()]
            ),
            new Chart(
                'Your Chart',
                (await new User('kaitl').get()).userId,
                '',
                'this is your chart',
                undefined,
                undefined,
                new Options(10, 10, 2, true, true),
                [new Card()]
            ),
        ];

        for (const chart of seeds) {
            await chart.create().catch(() => {});
        }
    }
}

Chart.createTable().then(() => {
    if (process.env.DEV) Chart.seed();
});
