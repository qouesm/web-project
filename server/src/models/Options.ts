export default class Options {
    id: string;
    height: number;
    width: number;
    margin: number;
    showTitles: boolean;
    showNumbers: boolean;

    constructor(
        height: number = 5,
        width: number = 5,
        margin: number = 4,
        showTitles: boolean = false,
        showNumbers: boolean = false
    ) {
        // this.id = Math.floor(Math.random() * 65536).toString(); // only for testing
        this.height = height;
        this.width = width;
        this.margin = margin;
        this.showTitles = showTitles;
        this.showNumbers = showNumbers;
    }
}