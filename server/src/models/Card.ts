const LastFM = require('last-fm');
const lastfm = new LastFM(process.env.LAST_FM_KEY);

import Database from '../utils/Database';

export default class Card {
    cardId: string;
    chartId: string;
    empty: boolean;
    title: string;
    artist: string;
    cover: string;

    constructor(
        title: string = '',
        artist: string = '',
        cover: string = '',
        chartId: string = '',
        cardId: string = ''
    ) {
        this.empty = !title;
        this.title = title;
        this.artist = artist;
        this.cover = cover;
        this.chartId = chartId;
        this.cardId = cardId;
    }

    static async createTable() {
        await Database.query(`
            CREATE TABLE IF NOT EXISTS cards (
                card_id VARCHAR(32) UNIQUE,
                title TEXT NOT NULL,
                artist TEXT NOT NULL,
                cover TEXT NOT NULL,
                CONSTRAINT card_pk PRIMARY KEY (card_id)
            )
        `);
    }
}

Card.createTable();
