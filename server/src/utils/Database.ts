import { Pool, PoolClient, Query, QueryResult } from 'pg';
import User from '../models/User';

import dotenv from 'dotenv';
dotenv.config();

const pool = new Pool({
    user: process.env.PG_USERNAME,
    password: process.env.PG_PASSWORD,
    host: 'localhost',
    database: process.env.PG_DATABASE,
    port: +(process.env.PG_PORT || 5432),
    connectionTimeoutMillis: 10000,
});

class Database {
    public con: Pool;

    constructor() {
        // try to connect to the database
        try {
            this.con = pool;
            // this.con.connect();
        } catch (err) {
            console.error('Could not connect to Postgres server', err);
        }
        console.log(
            `⚡️[server]: Postgres is running at http://localhost:${process.env.PG_PORT}`
        );
    }

    query(sql: string, values?: string[]): Promise<QueryResult<any>> {
        return new Promise((resolve, reject) => {
            if (values) {
                this.con.query(sql, values, (err, results) => {
                    if (err) reject(err);
                    else resolve(results);
                });
            } else {
                this.con.query(sql, (err, results) => {
                    if (err) reject(err);
                    else resolve(results);
                });
            }
        });
    }
}

export default new Database();
