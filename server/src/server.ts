import express from 'express';
import type { Express, Request, Response, NextFunction } from 'express';

import userRoute from './routes/User';
import lastfmRoute from './routes/LastFM';

const app: Express = express();
const port = process.env.EX_PORT || 3000;

app.use(express.json())
    .use(express.text())
    .use((_: Request, res: Response, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        );
        res.header(
            'Access-Control-Allow-Methods',
            'GET,POST,PUT,DELETE,OPTIONS'
        );
        next();
    })
 
    .use('/user', userRoute)
    .use('/lastfm', lastfmRoute);
    // .use((err: any, req: Request, res: Response, next: NextFunction) => {
    //     console.log(err);
    //     switch (err.code) {
    //         default:
    //             res.status(err.statusCode || 500).send({
    //                 error: err.message ?? 'Internal server error',
    //             });
    //     }
    // })

app.listen(port, () => {
    console.log(`⚡️[server]: Express  is running at http://localhost:${port}`);
});
