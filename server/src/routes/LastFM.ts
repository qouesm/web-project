import express from 'express';
import type { Request, Response } from 'express';

import dotenv from 'dotenv';
dotenv.config();

const LastFM = require('last-fm');
const lastfm = new LastFM(process.env.LAST_FM_KEY);

const router = express.Router();
router
    .post('/album/search', async (req: Request, res: Response) => {
        const request = JSON.parse(req.body);
        try {
            lastfm.albumSearch({ q: request.query }, (err: any, data: JSON) => {
                if (err) console.error(err);
                else res.status(200).json(data);
            });
        } catch (err) {
            console.error(err);
            res.status(401).send({ message: err });
        }
    })
    .post('/album/info', async (req: Request, res: Response) => {
        const request = JSON.parse(req.body);
        try {
            lastfm.albumInfo(
                { q: { artistName: request.artist, name: request.album } },
                (err: any, data: JSON) => {
                    if (err) console.error(err);
                    else res.status(200).json(data);
                }
            );
        } catch (err) {
            console.error(err);
            res.status(401).send({ message: err });
        }
    });
export default router;
