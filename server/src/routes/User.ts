import express from 'express';
import type { Request, Response } from 'express';
import User from '../models/User';
import Chart from '../models/Chart';
import Options from '../models/Options';

const router = express.Router();
router
    .get('/test', (_: Request, res: Response) => {
        console.log('test route');
        try {
            res.json(new User('andrw').get());
        } catch (err) {
            res.status(401).send({ message: err });
        }
    })

    .post('/signin', async (req: Request, res: Response) => {
        try {
            const reqJSON = JSON.parse(req.body);
            const reqUser: User = new User(reqJSON.username, reqJSON.password);
            const user: User = await reqUser.signIn();
            res.status(200).json({
                ...user,
                password: undefined,
                // user_id: undefined,
                created_at: undefined,
                message: '',
            });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: err });
        }
    })

    .post('/register', async (req: Request, res: Response) => {
        try {
            const reqJSON = JSON.parse(req.body);
            const reqUser: User = new User(reqJSON.username, reqJSON.password);
            const user: User = await reqUser.create();
            res.status(200).json({
                ...user,
                password: undefined,
                // user_id: undefined,
                created_at: undefined,
                message: '',
            });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: err });
        }
    })

    .post('/delete', async (req: Request, res: Response) => {
        try {
            const reqJSON = JSON.parse(req.body);
            const reqUser: User = new User(reqJSON.username, reqJSON.password);
            await reqUser.delete();
            res.status(200).send({ message: '' });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: err });
        }
    })

    .post('/get/id', async (req: Request, res: Response) => {
        try {
            const reqJSON = JSON.parse(req.body);
            const reqUser: User = new User(reqJSON.username);
            const user: User = await reqUser.get();
            res.status(200).json({
                userId: user.userId,
                message: '',
            });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: err });
        }
    })

    .post('/chart/save', async (req: Request, res: Response) => {
        try {
            const reqJSON = JSON.parse(req.body);
            const reqChart: Chart = new Chart(
                reqJSON.title,
                reqJSON.userId,
                '',
                reqJSON.description,
                reqJSON.created,
                reqJSON.edited,
                reqJSON.options,
                reqJSON.cards
            );

            const user: User = await new User('', undefined, reqChart.userId).get();

            await user.saveChart(reqChart);
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: err });
        }
    });

export default router;
