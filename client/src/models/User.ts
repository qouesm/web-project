import api from './Fetch';
import Chart from './Chart';
export default class User {
    userId?: string;
    username: string;
    password?: string;
    charts?: Chart[];

    constructor(name: string, password?: string, charts?: Chart[]) {
        this.username = name;
        this.password = password;
        this.charts = charts ?? [new Chart(`${name}'s chart`, '')];
    }

    async signIn() {
        await api('/user/signin', this).then((res) => {
            if (res.message) throw res.message;
            delete res.message;
            localStorage.setItem('user', JSON.stringify(res));
        });
    }

    static signOut() {
        localStorage.removeItem('user');
    }

    async register() {
        await api('/user/register', this).then((res) => {
            if (res.message) throw res.message;
            delete res.message;
            const user = new User(res.username);
            user.signIn();
        });
    }

    async delete() {
        await api('/user/delete', this).then((res) => {
            if (res.message) throw res.message;
            delete res.message;
            User.signOut();
        });
    }

    async getId(): Promise<string> {
        return await api('/user/get/id', this).then((res) => {
            if (res.message) throw res.message;
            delete res.message;
            return res.userId;
        });
    }

    async get() {
        throw 'Not implemented';
    }

    async saveChart(chart: Chart) {
        this.userId = await this.getId();

        await api('/user/chart/save', { ...chart, userId: this.userId }).then(
            (res) => {
                if (res.message) throw res.message;
                delete res.message;
            }
        );
    }
}
