export default class Options {
    id: string;
    height: number;
    width: number;
    margin: number;
    showTitles: boolean;
    showNumbers: boolean;

    constructor() {
        this.id = Math.floor(Math.random() * 65536).toString(); // only for testing
        this.height = 5;
        this.width = 5;
        this.margin = 4;
        this.showTitles = false;
        this.showNumbers = true;
    }
}
