import Card from './Card';
import Options from './Options';
import User from './User';

export default class Chart {
    chartId?: string;
    userId: string;
    title: string;
    description: string;
    created: Date;
    edited?: Date;
    cards: Card[];
    options: Options;

    constructor(
        title: string,
        userId: string,
        chartId: string = '',
        description: string = '',
        created: Date = new Date(),
        edited: Date | undefined = undefined,
        options: Options = new Options(),
        cards: Card[] = [new Card()]
    ) {
        this.chartId = chartId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.created = created;
        this.edited = edited;
        this.options = options;
        this.cards = cards;
    }

    // save to localstorage and database
    async update() {
        // save to localstorage
        localStorage.setItem('chart', JSON.stringify(this));

        // save to database
        if (!localStorage.getItem('user')) return;
        const u = JSON.parse(localStorage.getItem('user'));
        const user = new User(u.username, '');
        await user.saveChart(this);
    }
}
