export default class Card {
    cardId?: string;
    chartId: string;
    serialId: number;
    empty: boolean;
    title: string;
    artist?: string;
    cover?: string;

    // gives each card a unique id to be used
    // as a key for the flip animation to use.
    // must be incremented every time it is used
    static serialId: number = 0;

    constructor(title?: string, artist?: string, cover?: string, cardId?: string) {
        // this.cardId = Math.floor(Math.random() * 65536).toString(); // only for testing
        this.serialId = Card.serialId++;
        this.empty = !title;
        this.title = title ?? '';
        this.artist = artist ?? '';
        this.cover = cover ?? '';
        this.cardId = cardId ?? '';
    }
}
