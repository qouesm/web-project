export interface Drag {
    // what the mouse is dragging (first clicks on)
    cur: {
        n: number; // what card index was picked
        pos: string; // which grid are we in
    };
    // what the dragging mouse is hovering over currently
    hov: {
        n: number; // what card index was picked
        pos: string; // which grid are we in
    };
}
