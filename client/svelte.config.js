import adapter from '@sveltejs/adapter-auto';
import sveltePreprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
    kit: {
        adapter: adapter(),
        vite: {
            server: {
                fs: {
                    allow: ['..'],
                },
            },
            resolve: {
                alias: {
                    Buffer2: 'buffer',
                },
            },
        },
    },
    files: {
        assets: 'static',
    },
    preprocess: sveltePreprocess(),
};

export default config;
